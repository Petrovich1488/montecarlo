import java.util.ArrayList;
import java.util.Random;

public class Main2 {
    static final int N1 = 10_000;
    static final int N2 = 1_000_000;

    public static void main(String[] args) {
        new Main2().calculate(N1);
        new Main2().calculate(N2);
    }

    final double t = 1.96;
    final double wolframVolume = 1.18775;
    final double from = 1;
    final double to = 8;
    final Random rnd = new Random(22);

    double rand(double from, double to) {
        return from + rnd.nextDouble() * (to - from);
    }

    double f(double x) {
        return (1 / Math.sqrt(1 + Math.pow(x, 3))) * (to - from);
    }

    private void calculate(int n) {
        ArrayList<Double> points = new ArrayList<>();
        double sum = 0;
        for (int i = 0; i < n; i++) {
            double x = rand(from, to);
            double y = f(x);
            points.add(x);
            sum += y;
        }
        double volume = sum / n;
        double sumSquares = 0;
        for (double x : points) {
            sumSquares += Math.pow((volume - f(x)), 2);
        }
        double dispersion = Math.sqrt(sumSquares / (n - 1));
        double error = t * dispersion / Math.sqrt(n);
        double from = volume - error;
        double to = volume + error;
        System.out.println(String.format("Объем выборки n: %d", n));
        System.out.println(String.format("Значение интеграла: %.8f", volume));
        System.out.println(String.format("Значение интеграла с wolframalpha: %.8f", wolframVolume));
        System.out.println(String.format("Дисперсия: %.8f", dispersion));
        System.out.println(String.format("Погрешность: %.8f", error));
        System.out.println(String.format("Доверительный интервал: [%.8f, %.8f]", from, to));
    }
}
