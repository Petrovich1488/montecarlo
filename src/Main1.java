import java.util.Random;

public class Main1 {
    static final int N1 = 10_000;
    static final int N2 = 1_000_000;

    public static void main(String[] args) {
        new Main1().calculate(N1);
        new Main1().calculate(N2);
    }

    final double a = 2;
    final int k = 3;
    final double c = 4.3;
    final double t = 1.96;

    final Random rnd = new Random(22);

    double rand(double from, double to) {
        return from + rnd.nextDouble() * (to - from);
    }

    double f(double x) {
        return Math.pow(a, x);
    }

    private void calculate(int n) {
        double inside = 0;
        for (int i = 0; i < n; i++) {
            double sum = 0;
            for (int j = 0; j < k; j++) {
                double x = rand(0, 1);
                double y = f(x);
                sum += y;
            }
            if (sum <= c) inside++;
        }
        double volume = inside / n;
        double dispersion = Math.sqrt((inside * (1 - volume)) / (n - 1));
        double error = t * dispersion / Math.sqrt(n);
        double from = volume - error;
        double to = volume + error;
        System.out.println(String.format("Объем выборки n: %d", n));
        System.out.println(String.format("Объем части тела: %.8f", volume));
        System.out.println(String.format("Дисперсия: %.8f", dispersion));
        System.out.println(String.format("Погрешность: %.8f", error));
        System.out.println(String.format("Доверительный интервал: [%.8f, %.8f]", from, to));
    }
}
